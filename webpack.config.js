const abspath = require('path').join.bind(null, __dirname);
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = env => ({
    entry: [
        './index.js',
        './style.css',
    ],
    output: {
        path: abspath('build'),
        filename: "bundle.js",
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader"
                })
            },
            {
                test: /index.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'string-replace-loader',
                        query: {
                            search: '$$$wshostpath$$$',
                            replace: (env && env.wshostpath) || 'ws://localhost:1888',
                            strict: true,
                        }
                    },
                    'babel-loader'
                ],
            },
            {
                test: /\.js(x?)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
        ]
    },
    plugins: [
        new CopyWebpackPlugin([
            {from: 'node_modules/bootstrap/dist/css/bootstrap.min.css*', flatten: true},
            {from: 'node_modules/bootstrap/dist/js/bootstrap.bundle.min*', flatten: true},
            {from: 'node_modules/jquery/dist/jquery.min.js', flatten: true},
            {from: 'index.html', flatten: true},
        ]),

        new ExtractTextPlugin("styles.css"),
    ],
    devServer: {
        contentBase: abspath('build')
    }
});
