import * as querystring from 'querystring';
import { EventEmitter } from 'events';
import ReactDOM from 'react-dom';
import React from 'react';
import {Chat} from './chat.jsx';
import {Questions} from './aa.jsx';
import * as delta from './delta.js';

class Ws extends EventEmitter {
    constructor (url) {
        super();
        this.url = url;
        this._status = null;
        this.queue = [];
        this.reconnect();
    }

    reconnect () {
        const self = this;
        if (!!self.ws) {
            self.ws.close();
        }
        self._status = 'connecting';
        self.ws = new WebSocket(self.url);
        self.ws.addEventListener('open', function () {
            console.log('open');
            self._status = 'established';
            self._flush();
            self.emit('open');
        });

        self.ws.addEventListener('message', function (event) {
            console.log('got message:', event.data);

            self.emit('message', JSON.parse(event.data));
        });
    }

    send (msg) {
        const self = this;
        self.queue.push(JSON.stringify(msg));
        if (self._status === 'established') {
            self._flush();
        }
    }

    _flush () {
        const self = this;
        for (const msg of self.queue) {
            self.ws.send(msg);
        }
        self.queue = [];
    }

    onOpenOnce (cb) {
        const self = this;
        if (self._status === 'established') {
            cb();
        } else {
            self.once('open', cb);
        }
    }
}


$(function () {
    // see chat.jsx comment
    (function (e) {
        e.height(''+e.height()+'px');
    })($('#chat-panel'));

    //fixme: start initiating before DOM ready
    const {CID,SID} = querystring.parse(document.location.hash.slice(1));
    if (!CID || !SID) {
        return;
    }
    const wss = new Ws(`$$$wshostpath$$$/?SID=${SID}&CID=${CID}`);

    
    var qs, chat;
    ReactDOM.render(<Questions ref={instance=>qs=instance}/>,
                    document.getElementById('questions'));
    ReactDOM.render(<Chat ref={instance=>chat=instance}/>, 
                    document.getElementById('chat-panel'));

    $('button#next').click(function () {
        wss.send({type: 'next'});
    });

    wss.onOpenOnce(()=>chat.addNotification('Connected to server;'));

    wss.on('message', function (o) {
        if (o.type==='peerOnline') {
            chat.addNotification('The other party is online;');
        } else if (o.type==='ackChat') {
            chat.ack(o.id);
        } else if (o.type==='text') {
            chat.addPeerText(o.payload, o.timestamp);
        } else if (o.type==='gotoQuestion') {
            chat.addNotification('Next question;');
            qs.next();
        } else if (o.type==='next') {
            chat.addNotification('The other party wants to move to next question;');
        } else if (o.type==='ackNext') {
            chat.addNotification('Notified the other party to move to the next question;');
        } else if (o.type==='editAnswer') {
            const $t = $('textarea#their-answer');
            if (o.payload.version !== theirAnswerVersion) {
                $t.val('Error: version mismatch');
                return;
            }
            const theirAnswer = $t.val();
            const newAnswer = delta.patch(theirAnswer, o.payload.diff);
            $t.val(newAnswer);
            theirAnswerVersion += 1;
            console.log('"%s" "%s" -> "%s"',theirAnswer, o.payload.diff,  newAnswer);
        }
    });

    chat.events.on('send', function (text, id) {
        wss.send({type: 'text', id:id, payload: text});
    });

    var theirAnswerVersion = 0;
    var myAnswerVersion = 0;
    var myAnswer = '';

    $('textarea#my-answer').bind('input propertychange', function () {
        const newAnswer = $(this).val();
        const diff = delta.diff(myAnswer, newAnswer);
        wss.send({type: 'editAnswer', payload: {diff:diff, version: myAnswerVersion}});
        myAnswer = newAnswer;
        myAnswerVersion += 1;
    });
});
