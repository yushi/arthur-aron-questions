const jsdiff = require('diff');

exports.diff = function (s1, s2) {
    if (s2.startsWith(s1)) {
        return '+'+s2.slice(s1.length);
    }

    var i = 0;
    var edit = [];

    for (const part of jsdiff.diffChars(s1, s2)) {
        if (part.added) {
            edit.push(i);
            edit.push(part.value);
        } else if (part.removed) {
            edit.push(i);
            edit.push(part.value.length);
            i += part.value.length;
        } else {
            i += part.value.length;
        }
    }

    return JSON.stringify(edit);
};

exports.patch = function (s1, d) {
    if (d[0]==='+') {
        return s1+d.slice(1);
    }

    const edit = JSON.parse(d);
    const s = [...s1];

    for (var i=0; i<edit.length; i+=2) {
        if (typeof edit[i+1] === 'string') {
            s.splice(edit[i], 0, edit[i+1]);
        } else {
            s.splice(edit[i], edit[i+1]);
        }
    }

    return s.join('');
};
