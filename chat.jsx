/*
In order for the UI to render correctly, the parent of <Chat> must have height set. 
*/
//assuming jquery $ in global namesapce
import EventEmitter from 'events';
import React from 'react';
import './chat.css';

function Msg (props) {
    const pos = props.self?'right':'left';

    //relying on the CSS: .msg-row.right { flex-direction: row-reverse;}
    if (props.type==='text') {
        return (<div className={'msg-row '+pos}>
                  <div className={'py-0 callout '+pos}>{props.children}</div>
                  { (props.status==='sending') ?
                    <small className="loader">sending...</small> : '' }
                </div>);
    } else if (props.type==='notification') {
        return <div className="msg-row noti"><div className="alert alert-info py-0">{props.children}</div></div>
    }
}

/*
Events from Chat.events
send (text, id)
*/
export class Chat extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            messages: []
        };
        this.events = new EventEmitter;
        this.handleSend = this.handleSend.bind(this);
        this.textInput = null; // DOM element for <input>
        this.nextId = 0;
    }

    handleSend () {
        const text = this.textInput.value;
        this.textInput.value = '';
        const id = this.addMessage({from: 'me', type: 'text', payload: text, status: 'sending'});
        this.events.emit('send', text, id);
    }

    ack (id) {
        this.setState(function (prevState, _props) {
            const ms = prevState.messages;
            for (var i=ms.length-1; i>=0; i--) {
                if (ms[i].status==='sending') {
                    ms[i].status = null;
                } else {
                    break;
                }
            }
            return prevState;
        });
    }

    addMessage (fields) {
        const id = this.nextId++;

        this.setState(function (prevState, _props) {
            const prevMessages = prevState.messages;
            
            return {messages: prevMessages.concat({
                id: id,
                /*...fields*/
                from: fields.from,
                type: fields.type,
                payload: fields.payload,
                timestamp: fields.timestamp,
                status: fields.status,
            })};
        });

        const d = $('div.msg-stream');
        d.scrollTop(d.prop('scrollHeight')-d.prop('clientHeight'));

        return id;
    }

    addNotification (text) {
        this.addMessage({type: 'notification', payload: text});
    }

    addPeerText (text, timestamp) {
        this.addMessage({from: 'peer', type: 'text', payload: text, timestamp: timestamp});
    }

    render () {
        return (
          <div className="chat-panel">
            <div className="msg-stream mt-2">
            {
                this.state.messages.map(({id, from, payload, status, type})=>
                      <Msg type={type} status={status} key={id} self={from==='me'}>{payload}</Msg>
                )
            }
            </div>
            <div className="chat-input input-group">
              <input className="form-control" type="text" id="chat-input"
                     ref={elem=>this.textInput=elem}/>
              <div className="input-group-append">
                <button className="btn btn-outline-secondary py-0" type="button"
                        onClick={this.handleSend}>Send</button>
              </div>
            </div>
          </div>

        );
    }
}
