const url = require('url');
const WebSocket = require('ws');

class Session {
    constructor () {
        /*
          {ws: WebSockt,
          next: Boolean
          }
         */
        this.clients = {};

        this.currentQuestion = 0;
    }

    join (cid, ws) {
        const self = this;
        const client = self.clients[cid] || 
            (self.clients[cid]={});
        const oldWs = client.ws;
        if (!!oldWs) {
            oldWs.send(JSON.stringify({type: 'close', reason: 'newConnection'}));
            oldWs.close();
        }
        client.ws = ws;

        if (Object.values(self.clients).filter(c=>!!c.ws).length===2) {
            console.log('both peers online');
            self._sendAll({type: 'peerOnline'});
        }
        this._wire(cid, ws);
    }

    _sendAll (o) {
        const self = this;
        for (const {ws:s} of Object.values(self.clients)) {
            s.send(JSON.stringify(o));
        }
    }

    _getPeer (cid) {
        const self = this;
        for (var id in self.clients) {
            if (id!==cid) {
                return self.clients[id];
            }
        }
    }

    _sendPeer (cid, o) {
        const self = this;
        const peer = self._getPeer(cid);
        if (!peer) {
            return;
        }
        const peerWs = peer.ws;
        if (!!peerWs) {
            peerWs.send(JSON.stringify(o));
        }
    }
    _wire (cid, ws) {
        const self = this;
        ws.on('message', function (msg) {
            console.log('received: ', msg);
            const o = JSON.parse(msg);
            if (o.type === 'text') {
                ws.send(JSON.stringify({type: 'ackChat', id: o.id}));
                self._sendPeer(cid, {type: 'text', payload: o.payload});
            } else if (o.type === 'next') {
                self.clients[cid].next = true;
                if (self._getPeer(cid) && self._getPeer(cid).next) {
                    self.clients[cid].next = false;
                    self._getPeer(cid).next = false;
                    self.currentQuestion += 1;
                    self._sendAll({type: 'gotoQuestion', index: self.currentQuestion});
                } else {
                    ws.send(JSON.stringify({type: 'ackNext'}));
                    self._sendPeer(cid, {type: 'next'});
                }
            } else if (o.type === 'editAnswer') {
                self._sendPeer(cid, {type: o.type, payload: o.payload});
            }
        });

        ws.on('close', function () {
            console.log('close', cid);
            self.clients[cid].ws = null;
        });
    }   
}

const idSessionMapping = {};

const wss = new WebSocket.Server({ port: 1888 });

wss.on('connection', function connection(ws, req) {
    console.log('new connection', req.url);
    const {SID:sid, CID:cid} = url.parse(req.url, true).query;
    if (!sid || !cid) {
        ws.close();
        return;
    }

    const session = idSessionMapping[sid] || (idSessionMapping[sid]=new Session);

    session.join(cid, ws);

    ws.on('close', function () {
        console.log('close');
    });

    ws.on('error', function (err) {
        console.log('error:', err.message);
    });
});

/*
wtf:
events.js:136
      throw er; // Unhandled 'error' event
      ^

Error: read ECONNRESET
    at _errnoException (util.js:1031:13)
    at TCP.onread (net.js:619:25)

*/
